﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum Movement 
    {
    Left = 0,
    Right = 1,
    Up = 2,
    Down = 3,
    None = -1
    };
public class  SwipeDetector : TouchDetector
{
    // Start is called before the first frame update
//Enum
    private Vector2 fingerStart;
    private Vector2 fingerEnd;
    public List<Movement> movements = new List<Movement>();

    public override void OnTouchBegan(Touch touch) 
    {
        base.OnTouchBegan(touch);
        fingerStart = touch.position;
        fingerEnd = touch.position;
    } 

    public override void OnTouchMoved(Touch touch) 
    {
        base.OnTouchMoved(touch);
        fingerEnd = touch.position;
        if (Mathf.Abs(fingerStart.x - fingerEnd.x) >
        Mathf.Abs(fingerStart.y - fingerEnd.y)) 
        {
            if ((fingerEnd.x - fingerStart.x) > 0)
            movements.Add(Movement.Right);
            else
            movements.Add(Movement.Left);
        } else 
        {
            if ((fingerEnd.y - fingerStart.y) > 0)
            movements.Add(Movement.Up);
            else
            movements.Add(Movement.Down);
        }
        fingerStart = touch.position;
    }

    public override void OnTouchEnded(Touch touch) 
    {
        base.OnTouchEnded(touch);
        print ("Direction is : "+ SumDirection ());
        fingerStart = Vector2.zero;
        fingerEnd = Vector2.zero;
        movements.Clear();
    }
    
    Movement SumDirection() 
    {
        Movement[] sum = new Movement[4];
        foreach (Movement move in movements) 
        {
            sum [(int)move]++;
        }
        for (int i = 0; i < sum.Length; i++) 
        {
            if (sum [i] == sum.Max ()) 
            {
                return (Movement)i;
            }
        }
        return Movement.None;
    }

}
