﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class CharcacterController : MonoBehaviour {
    public PlatformerCharacter2D m_Character;
    public ButtonState _JumpButton;
    public ButtonState _RightButton;
    public ButtonState _LeftButton;
    public bool isPass = false;
    private bool m_Jump;
    private bool m_Right;
    private bool m_Left;
    public float speed = 0.5f;
    // Update is called once per frame
    void Update () {
        if (_JumpButton._currentState == ButtonState.State.Down &&
            m_Jump == false && isPass == false) {
            m_Jump = true;
            isPass = true;
        } else if (_JumpButton._currentState == ButtonState.State.Up &&
            isPass == true) {
            isPass = false;
        }
        if (_RightButton._currentState == ButtonState.State.Down &&
            m_Right == false) {
            m_Right = true;
        } else if (_RightButton._currentState == ButtonState.State.Up &&
            m_Right == true) {
            m_Right = false;
        }
        if (_LeftButton._currentState == ButtonState.State.Down &&
            m_Left == false) {
            m_Left = true;
        } else if (_LeftButton._currentState == ButtonState.State.Up &&
            m_Left == true) {
            m_Left = false;
        }

    }
    private void FixedUpdate () {
        float move = 0;
        if (m_Right == true) {
            move = speed;
        }
        if (m_Left == true) {
            move = -speed;
        }

        m_Character.Move (move, false, m_Jump);
        m_Jump = false;
    }
}