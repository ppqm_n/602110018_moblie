﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class PlayerwithJoyStick : MonoBehaviour {
    public PlatformerCharacter2D m_Character;
    public VariableJoystick variableJoystick;
    public ButtonState _JumpButton;
    public bool isPress = false;
    private bool m_Jump;

    void Update () {
        if (_JumpButton._currentState == ButtonState.State.Down &&
            m_Jump == false && isPress == false) {
            m_Jump = true;
            isPress = true;
        } else if (_JumpButton._currentState == ButtonState.State.Up &&
            isPress == true) {
            isPress = false;
        }
    }
    public void FixedUpdate () {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical +
            Vector3.right * variableJoystick.Horizontal;

        m_Character.Move (direction.x, false, m_Jump);
        m_Jump = false;
    }
}