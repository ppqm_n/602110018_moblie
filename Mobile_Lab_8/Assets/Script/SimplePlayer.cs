﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlayer : MonoBehaviour
{
    Animator animatior;
    
    // Start is called before the first frame update
    void Start()
    {
        animatior = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        bool fire = Input.GetButtonDown("Fire1");

        animatior.SetFloat("Forward",v);
        animatior.SetFloat("Strafe",h);
        animatior.SetBool("Fire",fire);
    }
}
