﻿using UnityEngine;
using System.Collections;

public enum OppositeSide {
	None,
	Left,
	Right,
	Top,
	Bottom
}

public class SwitchSides : MonoBehaviour {
	public OppositeSide m_side = OppositeSide.None;
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag.Contains ("Player")) {
			Debug.Log(other.GetComponent<ShipControl>());
			other.GetComponent<ShipControl> ().FlipPosition (m_side);
		} else if (other.tag.Contains ("Laser")) {
			Destroy (other.gameObject);
		}
	}
}
