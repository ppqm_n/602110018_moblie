﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intersects2DEvent : MonoBehaviour
{
    public Collider2D m_objectA;
    public Collider2D m_objectB;
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_objectA.bounds.Intersects (m_objectB.bounds)) 
        {
            Debug.Log ("Object A Intersects Object B");
        }
    }
}
