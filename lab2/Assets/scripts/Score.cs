﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public int ballValue;
    private int score;
    void Start () 
    {
        score = 0;
        UpdateScore ();
    }

    void OnTriggerEnter2D (Collider2D other) 
    {
        score += ballValue;
        UpdateScore ();
    }

    void UpdateScore () 
    {
        Debug.Log("SCORE:\n" + score);
    }
    // Start is called before the first frame update
    
}
